export const initialState = {
	data: [],
}

export function reducer(state, action) {
	switch (action.type) {
		case 'FETCH_DATA':
			return {
				...state,
				data: action.payload,
			}

		case 'DELETE_DATA':
			return {
				...state,
				data: action.payload,
			}
		case 'ADD_DATA':
			return {
				...state,
				data: [...state.data, action.payload],
			}
		case 'UPDATE_DATA':
			return {
				...state,
				data: [
					...state.data.map((item) => {
						return item.id == action.payload.id ? { ...action.payload } : item
					}),
				],
			}
	}
}
