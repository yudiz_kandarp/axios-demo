import { instance } from '../Axios'
/**
 *
 * @param {function} dispatch
 */

export function fetchData(dispatch) {
	instance
		.get('/posts')
		.then((res) => dispatch({ type: 'FETCH_DATA', payload: res.data }))
}
export function DeleteData(id, dispatch, state) {
	instance
		.delete(`/posts/${id}`)
		.then((req) => {
			if (req.status !== 200) return
			dispatch({
				type: 'DELETE_DATA',
				payload: state.data.filter((item) => id != item.id),
			})
			console.log('deleted successfully')
		})
		.catch((err) =>
			console.log(
				`error while deleting 
    not actually error happening because in database our newly create entry is not store so this api call can't delete that.
    `,
				err
			)
		)
}
