import React, { createContext, useContext, useEffect, useReducer } from 'react'
import PropTypes from 'prop-types'
import { initialState, reducer } from '../Reducers/Reducer'
import { fetchData } from '../Components'
const Global = createContext()

function GlobalContext({ children }) {
	const [state, dispatch] = useReducer(reducer, initialState)

	useEffect(() => {
		fetchData(dispatch)
	}, [])
	return (
		<Global.Provider
			style={{ display: 'relative' }}
			value={{ state, dispatch }}
		>
			{children}
		</Global.Provider>
	)
}

export function useGlobal() {
	return useContext(Global)
}

GlobalContext.propTypes = {
	children: PropTypes.node,
}
export default GlobalContext
