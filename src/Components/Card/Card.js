import React from 'react'
import PropTypes from 'prop-types'
import { Button } from '../'
import { DeleteData, useGlobal } from '../'
import { useNavigate } from 'react-router-dom'
function Card({ item }) {
	const { state, dispatch } = useGlobal()
	const navigate = useNavigate()
	return (
		<div className='card'>
			<div className='card_design'></div>
			<div className='card_title'>{item.title}</div>
			<div
				className='card_description'
				onClick={() => navigate(`/post/${item.id}`)}
			>
				{item.body}
			</div>
			<div className='card_button'>
				<Button warning onClick={() => navigate(`/update/${item.id}`)}>
					update
				</Button>
				<Button danger onClick={() => DeleteData(item.id, dispatch, state)}>
					delete
				</Button>
			</div>
		</div>
	)
}
Card.propTypes = {
	item: PropTypes.object,
}
export default Card
