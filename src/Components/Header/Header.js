import React from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import { Button } from '../index'

function Header() {
	const navigate = useNavigate()
	const location = useLocation()

	return (
		<header>
			<h2> Axios</h2>
			<Button
				onClick={() =>
					navigate(`${location.pathname == '/' ? '/add.html' : '/'}`)
				}
			>
				{location.pathname == '/' ? 'add' : 'Home'}
			</Button>
		</header>
	)
}

export default Header
