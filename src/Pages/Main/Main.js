import React from 'react'
import { Card } from '../../Components'
import { useGlobal } from '../../Components'

function Main() {
	const { state } = useGlobal()
	return (
		<div className='main'>
			<div className='card_containter'>
				{state.data?.length == 0 && <h1>Loading...</h1>}
				{state.data?.map((item) => {
					return <Card key={item.id} item={item} />
				})}
			</div>
		</div>
	)
}

export default Main
