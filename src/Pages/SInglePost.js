import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { instance } from '../Axios'
import { Button, DeleteData, useGlobal } from '../Components'

function SInglePost() {
	const [post, setPost] = useState('')
	const { state, dispatch } = useGlobal()
	const { postid: id } = useParams()
	const navigate = useNavigate()
	useEffect(() => {
		if (id <= 100) {
			instance.get(`posts/${id}`).then((res) => setPost(res.data))
		} else {
			setPost(state.data.find((item) => id == item.id))
		}
	}, [id])
	return (
		<>
			<div className='singlepost_containter'>
				{post == '' ? (
					<h1>Loading...</h1>
				) : (
					<div className='card fullsize'>
						<div className='card_design'></div>
						<div className='card_title'>{post.title}</div>
						<div className='card_description'>{post.body}</div>
						<div className='card_button'>
							<Button warning onClick={() => navigate(`/update/${post.id}`)}>
								update
							</Button>
							<Button
								danger
								onClick={() => {
									DeleteData(post.id, dispatch, state)
									navigate('/')
								}}
							>
								delete
							</Button>
						</div>
					</div>
				)}
			</div>
		</>
	)
}

export default SInglePost
