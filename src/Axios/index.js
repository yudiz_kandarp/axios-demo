import axios from 'axios'

export const instance = axios.create({
	baseURL: 'https://jsonplaceholder.typicode.com/',
})

instance.interceptors.request.use(
	function (config) {
		console.log('Hey there, i see, you are trying to get posts')
		return config
	},
	function (error) {
		return Promise.reject(error)
	}
)

instance.interceptors.response.use(
	function (response) {
		if (response.config.method == 'delete' && response.status == 200)
			console.log(response)
		else console.log('Hey there, here you go with your data, enjoy !')
		return response
	},
	function (error) {
		return Promise.reject(error)
	}
)
