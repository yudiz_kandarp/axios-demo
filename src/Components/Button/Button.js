import React from 'react'
import PropTypes from 'prop-types'
function Button({
	children,
	primary,
	style,
	danger,
	warning,
	addClass,
	onClick,
}) {
	return (
		<button
			onClick={onClick}
			style={style}
			className={`btn ${addClass} ${primary ? 'primary' : ''} ${
				danger ? ' danger' : ''
			} ${warning ? ' warning' : ''}
			}`}
		>
			{children}
		</button>
	)
}
Button.propTypes = {
	onClick: PropTypes.func,
	children: PropTypes.node,
	style: PropTypes.object,
	primary: PropTypes.bool,
	danger: PropTypes.bool,
	warning: PropTypes.bool,
	addClass: PropTypes.string,
}

export default Button
