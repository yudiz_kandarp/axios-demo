import React from 'react'
import { GlobalContext, Header } from './Components'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import AddPost from './Pages/AddPost'
import Main from './Pages/Main/Main'
import SInglePost from './Pages/SInglePost'
import UpdatePost from './Pages/UpdatePost'
function App() {
	return (
		<GlobalContext>
			<Router>
				<Header />
				<Routes>
					<Route path='/' exact element={<Main />} />
					<Route path='/add.html' element={<AddPost />} />
					<Route path='/post/:postid' element={<SInglePost />} />
					<Route path='/update/:postid' element={<UpdatePost />} />
					<Route path='*' element={<div>Page Not Found</div>} />
				</Routes>
			</Router>
		</GlobalContext>
	)
}

export default App
