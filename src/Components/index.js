import Card from './Card/Card'
import Header from './Header/Header'
import Button from './Button/Button'

export { Card, Header, Button }

/* importing context and axios */
import GlobalContext, { useGlobal } from '../Contexts/GlobalContext'
import { instance } from '../Axios'
export { GlobalContext, useGlobal, instance }

/* helper methods */
import { fetchData, DeleteData } from '../Utils/Helper'
export { fetchData, DeleteData }
/* importing style sheets */

import '../Styles/card.scss'
import '../Styles/header.scss'
import '../Styles/button.scss'
import '../Styles/main.scss'
import '../Styles/addPost.scss'
import '../Styles/singlepost.scss'
