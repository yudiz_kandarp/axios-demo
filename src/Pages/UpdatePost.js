import React, { useEffect, useRef } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { Button, useGlobal, instance } from '../Components'

function UpdatePost() {
	// const [post, setPost] = useState({})
	const navigate = useNavigate()
	const title = useRef(null)
	const body = useRef(null)
	const { state, dispatch } = useGlobal()
	const { postid: id } = useParams()
	useEffect(() => {
		if (id <= 100) {
			instance.get(`posts/${id}`).then((res) => {
				title.current.value = res.data.title
				body.current.value = res.data.body
			})
		} else {
			let data = state.data.find((item) => id == item.id)
			if (!data) navigate('/')
			else {
				title.current.value = data.title
				body.current.value = data.body
			}
		}
	}, [id])
	function submitPost(e) {
		e.preventDefault()
		if (title.current?.value.trim() == '' || body.current?.value.trim() == '')
			return
		if (id <= 100) {
			instance
				.put(`/posts/${id}`, {
					title: title.current.value,
					body: body.current.value,
				})
				.then((res) => {
					dispatch({
						type: 'UPDATE_DATA',
						payload: { ...res.data },
					})
					navigate('/')
				})
				.catch((err) => console.error('Error while updating Data :', err))
		} else {
			dispatch({
				type: 'UPDATE_DATA',
				payload: { id, title: title.current.value, body: body.current.value },
			})
			navigate('/')
		}
	}

	return (
		<section className='section'>
			<h1>Update Post</h1>
			<form className='input_container' onSubmit={submitPost}>
				<input
					ref={title}
					placeholder='title'
					className='input title_input'
					defaultValue={title.current?.value || ''}
					required
				/>
				<textarea
					ref={body}
					placeholder='body'
					className='input body_input'
					defaultValue={body.current?.value || ''}
					required
				/>
				<Button style={{ width: '30%' }} warning>
					add
				</Button>
			</form>
		</section>
	)
}

export default UpdatePost
