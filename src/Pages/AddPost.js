import React, { useRef } from 'react'
import { useNavigate } from 'react-router-dom'
import { Button, instance, useGlobal } from '../Components'

function AddPost() {
	const navigate = useNavigate()
	const title = useRef(null)
	const body = useRef(null)
	const { dispatch } = useGlobal()

	function submitPost(e) {
		e.preventDefault()
		if (title.current?.value.trim() == '' || body.current?.value.trim() == '')
			return
		instance
			.post('/posts', {
				title: title.current.value,
				body: body.current.value,
			})
			.then((res) => {
				if (res.status !== 201) return
				title.current.value = ''
				body.current.value = ''
				navigate('/')
				dispatch({
					type: 'ADD_DATA',
					payload: { ...res.data, id: new Date().valueOf },
				})
			})
			.catch((err) => console.error('Error while posting Data :', err))
	}

	return (
		<section className='section'>
			<h1>Add New Post</h1>
			<form className='input_container' onSubmit={submitPost}>
				<input
					ref={title}
					placeholder='title'
					className='input title_input'
					required
				/>
				<textarea
					ref={body}
					placeholder='body'
					className='input body_input'
					required
				/>
				<Button style={{ width: '30%' }} warning>
					add
				</Button>
			</form>
		</section>
	)
}

export default AddPost
